/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.constants;

public class MobileCardConstants {
	
	public static final int USUARIO_ACTIVO = 1;
	public static final int USUARIO_ACTIVO_2 = 99;
	public static final int USUARIO_BLOQUEADO = 3;
	public static final int USUARIO_RESET = 98;
	public static final char USUARIO_WS_ACTIVO = 'T';
	
	public static final int JUMIO_APPROVED = 1;
	public static final int JUMIO_WAITING = 2;
	public static final int JUMIO_DENIED = 3;
		
	public static final String JUMIO_API = "Jumio";
	public static final String SMS_API = "SMS";
	public static final String EMAIL_API = "Email";
	public static final String PUSH_API = "Push";
	public static final String IMG_API = "Image";
	
	public static final int ID_PAIS_MX = 1;
	public static final int ID_PAIS_COL = 2;
	public static final int ID_PAIS_EUA = 3;
	public static final int ID_PAIS_PER = 4;
	
	public static final int MAYORIA_EDAD = 18;
	
	public static final String SMS_PREFIX_MX = "+521";
	public static final String SMS_PREFIX_COL = "+57";
	public static final String SMS_PREFIX_EUA = "+1";
	public static final String SMS_PREFIX_PER = "+51";
	
	public static final String LANG_SPANISH = "ES";
	public static final String LANG_ENGLISH = "EN";
	
	public static final int RESPONSE_LENGTH = 100;
	
	public static final String EMAIL_TAG_NOMBRE = "@NOMBRE";
	public static final String EMAIL_TAG_USUARIO = "@USUARIO";
	public static final String EMAIL_TAG_PASSWORD = "@PASSWORD";
	public static final String EMAIL_TAG_URL = "@URL";
	public static final String EMAIL_TAG_ERROR = "@ERROR";
	
	public static final String APP_VERSION = "Addcel MobileCard/1.0";
	
}
