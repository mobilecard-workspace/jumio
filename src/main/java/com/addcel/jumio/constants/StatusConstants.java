/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.constants;

public class StatusConstants {

	//Codigos de error
	public static final int SUCCESS_CODE = 0;
	public static final int INTERNAL_ERROR_CODE = -1;
	public static final int JUMIO_NOT_AVAILABLE_CODE = -2;
	public static final int USER_NOT_FOUND_CODE = -3;
	
	//Mensajes de error
	public static final String INTERNAL_ERROR_MSG = "Su solicitud no pudo ser procesada, intentelo nuevamente. Codigo de error: JM" + INTERNAL_ERROR_CODE;
	public static final String JUMIO_NOT_AVAILABLE_MSG = "Tus documentos estan siendo validados. Para poder ingresar a tu aplicacion recibiras un sms y un correo, en un maximo de 5 minutos.";
	public static final String USER_NOT_FOUND_MSG = "Su solicitud no pudo ser procesada, intentelo nuevamente. Codigo de error: JM" + USER_NOT_FOUND_CODE;
//	public static final String INTERNAL_ERROR_MSG = "Ocurrio un error interno del servidor";
//	public static final String JUMIO_NOT_AVAILABLE_MSG = "No se pudo conectar con el servicio rest de Jumio";
//	public static final String USER_NOT_FOUND_MSG = "No existe ningun usuario con ese jumioReference";
	
}
