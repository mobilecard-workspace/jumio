/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.constants;

public class PushConstants {
	
	public static final String PUSH_TIPO_USUARIO = "USUARIO";
	public static final String PUSH_TIPO_NEGOCIO = "NEGOCIO";
	
	public static final int PUSH_SUCCESS = 0;
	
}
