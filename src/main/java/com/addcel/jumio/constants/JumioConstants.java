/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.constants;

public class JumioConstants {
	
	public static final String JUMIO_ID_SCAN_REFERENCE_KEY = "jumioIdScanReference";
	public static final String VERIFICATION_STATUS_KEY = "verificationStatus";
	public static final String ID_SCAN_STATUS_KEY = "idScanStatus";
	public static final String REJECT_REASON_KEY = "rejectReason";
	public static final String IDENTITY_VERIFICATION_KEY = "identityVerification";
	public static final String CALLBACK_DATE_KEY = "callbackDate";
	public static final String TRANSACTION_DATE_KEY = "transactionDate";
	public static final String MERCHANT_IDSCAN_REFERENCE_KEY = "merchantIdScanReference";
	public static final String ID_TYPE_KEY = "idType";
	public static final String CURP_KEY = "curp";
	public static final String ID_DOB_KEY = "idDob";
	public static final String FIRSTNAME_KEY = "idFirstName";
	public static final String LASTNAME_KEY = "idLastName";
	public static final String ID_NUMBER_KEY = "idNumber";
	public static final String ID_COUNTRY_KEY = "idCountry";
	public static final String IMG_BACK_KEY = "idScanImageBackside";
	public static final String IMG_FRONT_KEY = "idScanImage";
	
	public static final String COLOMBIA = "COL";
	public static final String PERU = "PER";
	public static final String USA = "USA";
	public static final String MEXICO = "MEX";
	
	public static final String APPROVED_VERIFIED = "APPROVED_VERIFIED";
	public static final String DENIED_FRAUD = "DENIED_FRAUD";
	public static final String SUCCESS_STATUS = "SUCCESS";
	public static final String ERROR_STATUS = "ERROR";
	public static final String MATCH_SIMILARITY = "MATCH";
	public static final String NO_MATCH_SIMILARITY = "NOT_MATCH";
	public static final String NOT_POSSIBLE_SIMILARITY = "NOT_POSSIBLE";
	public static final String TRUE_VALIDITY = "TRUE";
	public static final String FALSE_VALIDITY = "FALSE";
	
	public static final String DATE_FORMAT = "YYYY-MM-DD'T'hh:mm:ss.SSS'Z'";
	public static final String ID_DOB_FORMAT = "YYYY-MM-DD";
	
	public static final String URL_WORD_REPLACE = "<scanReference>";
	
	public static final String MERCHANT_USUARIO = "U-";
	public static final String MERCHANT_NEGOCIO = "N-";
	
}
