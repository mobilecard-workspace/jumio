/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.addcel.jumio.constants.JumioConstants;
import com.addcel.jumio.constants.MobileCardConstants;
import com.addcel.jumio.constants.PushConstants;
import com.addcel.jumio.constants.SMSConstants;
import com.addcel.jumio.domain.IdentityVerification;
import com.addcel.jumio.domain.RejectReason;
import com.addcel.jumio.entity.JumioValidations;
import com.addcel.jumio.entity.LcpfEstablecimiento;
import com.addcel.jumio.entity.TUsuarios;
import com.addcel.jumio.entity.TemplateEmail;
import com.addcel.jumio.repository.JumioValidationsRepository;
import com.addcel.jumio.repository.LcpfEstableciemientoRepository;
import com.addcel.jumio.repository.TUsuariosRepository;
import com.addcel.jumio.repository.TemplateEmailRepository;
import com.addcel.jumio.util.PropertiesFile;
import com.addcel.jumio.util.RestClient;
import com.addcel.jumio.ws.request.CorreoRequest;
import com.addcel.jumio.ws.request.PushRequest;
import com.addcel.jumio.ws.request.SMSParams;
import com.addcel.jumio.ws.request.SMSRequest;
import com.addcel.jumio.ws.response.PushResponse;
import com.addcel.jumio.ws.response.SMSResponse;
import com.addcel.utils.AddcelCrypto;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CallbackService {

	private static final Logger LOGGER = LogManager.getLogger(CallbackService.class);
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	@Autowired
	private JumioValidationsRepository jumioValRepo;
	
	@Autowired
	private TemplateEmailRepository templateEmailRepo;
	
	@Autowired
	private LcpfEstableciemientoRepository establecimientoRepo;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private RestClient restClient;
	
	public boolean processResult(String response, String ipAddres) {
		boolean success = false;
		int jumioStatus = MobileCardConstants.JUMIO_DENIED;
		String user = "";
		String pass = "";
		String nombre = "";
		String apellido = "";
		String correo = "";
        String telefono = "";
        String nip = "";
		int idPais = 0;
		TUsuarios usuario = null;
		LcpfEstablecimiento establecimiento = null;
		TemplateEmail template = null;
		String idMail = propsFile.getEmailRechazoId();
		String error = "";
		String idioma = MobileCardConstants.LANG_SPANISH;
		int idApp = 0;
		long idUser = 0;
		String tipoUsuario = "";
		
		List<String> ips = Arrays.asList(propsFile.getJumioIpAddresList().trim().split(";"));
		boolean validateIp = propsFile.getJumioValidateIp();
		
		LOGGER.info("Se validara la IP que esta realizando la peticion? " + validateIp);
		LOGGER.debug("Lista de IPs en la White List: " + Arrays.toString(ips.toArray()));
		
		if(validateIp && !ips.contains(ipAddres)) {
			LOGGER.warn("La IP no es de Jumio, no se continuara con el flujo");
			return success;
		} else {
			LOGGER.debug("La IP se encuentra en whitelist, se procede con el flujo");
		}
		
		ObjectMapper mapper = new ObjectMapper();
		LOGGER.debug("Resultado de Jumio sin parsear: " + response);
		
		List<NameValuePair> responseList = URLEncodedUtils.parse(response, Charset.forName("UTF-8"));
		
		Map<String, String> params = responseList.stream().collect(Collectors.toMap(NameValuePair::getName, NameValuePair::getValue));
		
		String jumioReference = params.get(JumioConstants.JUMIO_ID_SCAN_REFERENCE_KEY);
		jumioReference = jumioReference == null ? "" : jumioReference;
		String urlImgFront = params.get(JumioConstants.IMG_FRONT_KEY);
		String urlImgBack = params.get(JumioConstants.IMG_BACK_KEY);
		
		JumioValidations jumioVal = new JumioValidations();
		jumioVal.setCallbackDate(this.convertirFecha(params.get(JumioConstants.CALLBACK_DATE_KEY)));
		jumioVal.setIdScanStatus(params.get(JumioConstants.ID_SCAN_STATUS_KEY));
		jumioVal.setIdType(params.get(JumioConstants.ID_TYPE_KEY));
		jumioVal.setJumioIdScanReference(jumioReference);
		jumioVal.setMerchantIdScanReference(params.get(JumioConstants.MERCHANT_IDSCAN_REFERENCE_KEY));
		jumioVal.setResponse(response.substring(0, MobileCardConstants.RESPONSE_LENGTH));
		jumioVal.setTransactionDate(this.convertirFecha(params.get(JumioConstants.TRANSACTION_DATE_KEY)));
		jumioVal.setVerificationStatus(params.get(JumioConstants.VERIFICATION_STATUS_KEY));
		
		String idDob = params.get(JumioConstants.ID_DOB_KEY);
		String curp = params.get(JumioConstants.CURP_KEY);
		String firstName = params.get(JumioConstants.FIRSTNAME_KEY);
		String lastName = params.get(JumioConstants.LASTNAME_KEY);
		String idNumber = params.get(JumioConstants.ID_NUMBER_KEY);
		String idType = params.get(JumioConstants.ID_TYPE_KEY);
		String idCountry = params.get(JumioConstants.ID_COUNTRY_KEY);
		
		LOGGER.info("PARAMS: {} {} {} {} {} {} {}" ,idDob, curp, firstName, lastName, idNumber, idType, idCountry);
		
		if(jumioVal.getMerchantIdScanReference().startsWith(JumioConstants.MERCHANT_USUARIO)) {
			tipoUsuario = PushConstants.PUSH_TIPO_USUARIO;
		} else if(jumioVal.getMerchantIdScanReference().startsWith(JumioConstants.MERCHANT_NEGOCIO)) {
			tipoUsuario = PushConstants.PUSH_TIPO_NEGOCIO;
		}
		
		idUser = Long.parseLong(jumioVal.getMerchantIdScanReference().substring(2, jumioVal.getMerchantIdScanReference().length()));
				
		if(tipoUsuario.equals(PushConstants.PUSH_TIPO_USUARIO)) {
			LOGGER.info("Buscando usuario con jumio_reference: [{}] y id: [{}]", jumioReference, idUser);
			usuario = tUsuariosRepo.findByJumioReferenceOrIdUsuario(jumioReference, idUser);
		} else if(tipoUsuario.equals(PushConstants.PUSH_TIPO_NEGOCIO)) {
			LOGGER.info("Buscando establecimiento con jumio_reference: [{}] y id: [{}]", jumioReference, idUser);
			establecimiento = establecimientoRepo.findByJumioReferenceOrId(jumioReference, idUser);
		}
				
		if(usuario != null || establecimiento != null) {
			LOGGER.info("Usuario/establecimiento encontrado: {}", usuario != null ? usuario.toString() : establecimiento.toString());
			
			if((usuario != null && usuario.getJumioStatus() == MobileCardConstants.JUMIO_APPROVED) || 
					(establecimiento != null && establecimiento.getJumioStatus() == MobileCardConstants.JUMIO_APPROVED)) {
				LOGGER.warn("El usuario/establecimiento ya habia sido aprobado por Jumio, se finaliza flujo");
				return true;
			}
			
			try {
				if(params.get(JumioConstants.ID_SCAN_STATUS_KEY).equalsIgnoreCase(JumioConstants.SUCCESS_STATUS)) {
					LOGGER.info("Jumio ha validado que el documento escaneado es original");
					IdentityVerification identityVerification = mapper.readValue(params.get(JumioConstants.IDENTITY_VERIFICATION_KEY), IdentityVerification.class);
					jumioVal.setValidity(params.get(identityVerification.getValidity()));
					jumioVal.setSimilarity(identityVerification.getSimilarity());
					
					if(identityVerification.getValidity().equalsIgnoreCase(JumioConstants.TRUE_VALIDITY) &&
						identityVerification.getSimilarity().equalsIgnoreCase(JumioConstants.MATCH_SIMILARITY)) {
						
						LOGGER.info("Jumio confirma que la foto es correcta y coincide con el DOC escaneado");
						jumioStatus = MobileCardConstants.JUMIO_APPROVED;
						success = true;
						idMail = propsFile.getEmailRegistroId();
					} else if(identityVerification.getValidity().equalsIgnoreCase(JumioConstants.TRUE_VALIDITY) &&
						!identityVerification.getSimilarity().equalsIgnoreCase(JumioConstants.MATCH_SIMILARITY)) {
						
						LOGGER.warn("Jumio confirma que la foto no coincide con el DOC escaneado");
						error = propsFile.getErrorMsgMatch();
					} else if(identityVerification.getValidity().equalsIgnoreCase(JumioConstants.FALSE_VALIDITY)) {
						LOGGER.warn("Jumio no pudo reconocer a la persona: " + identityVerification.getReason());
						jumioVal.setReason(identityVerification.getReason());
						error = propsFile.getErrorMsgPhoto();
					}
				} else {
					LOGGER.warn("Jumio confirma que el documento no es original: " + params.get(JumioConstants.VERIFICATION_STATUS_KEY));
					error = propsFile.getErrorMsgDoc();
					
					if(params.get(JumioConstants.REJECT_REASON_KEY) != null) {
						RejectReason rejectReason = mapper.readValue(params.get(JumioConstants.REJECT_REASON_KEY), RejectReason.class);
						
						jumioVal.setRejectReasonCode(rejectReason.getRejectReasonCode());
						jumioVal.setRejectReasonDescription(rejectReason.getRejectReasonDescription());
						
						LOGGER.warn("Codigo del rechazo: " + rejectReason.getRejectReasonCode());
						LOGGER.warn("Descripcion del rechazo: " + rejectReason.getRejectReasonDescription());
					}
				}
				
				//Se obtiene las fotos escaneadas del documento
				String base64Front = (String) restClient.createRequest(null, urlImgFront, HttpMethod.GET, MobileCardConstants.IMG_API);
				String base64Back = (String) restClient.createRequest(null, urlImgBack, HttpMethod.GET, MobileCardConstants.IMG_API);
				
				jumioVal.setDocImgFront(base64Front);
				jumioVal.setDocImgBack(base64Back);
				
				SimpleDateFormat sdf = new SimpleDateFormat(JumioConstants.ID_DOB_FORMAT);
				Date idDobDate = sdf.parse(idDob);
				
				LocalDate ahora = LocalDate.now();
				LocalDate inicio = idDobDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
								
				if(usuario != null) {
					Period periodo = Period.between(inicio, ahora);
					LOGGER.debug("Edad del usuario: {} anios, {} meses y {} dias", periodo.getYears(), periodo.getMonths(), periodo.getDays());
					
					if(periodo.getYears() < MobileCardConstants.MAYORIA_EDAD) {
						LOGGER.warn("El usuario que se intenta registrar es menor de edad");
						
						idMail = propsFile.getEmailRechazoId();
						error = propsFile.getEmailMensajeEdad();
						success = false;
						jumioStatus = MobileCardConstants.JUMIO_DENIED;
					} else {
						LOGGER.debug("El usuario es mayor de edad");
					}
										
					usuario.setJumioStatus(jumioStatus);
					usuario.setJumioReference(jumioReference);
					
					usuario.setUsrNombre(firstName != null ? firstName : usuario.getUsrNombre());
					usuario.setUsrApellido(lastName != null ? lastName : usuario.getUsrApellido());
					usuario.setUsrFechaNac(idDob != null ? new java.sql.Date(idDobDate.getTime()) : usuario.getUsrFechaNac());
					usuario.setDocumentNumber(idNumber);
					usuario.setDocumentType(idType);
					
					LOGGER.debug("El documento escaneado es de: " + idCountry);
					
					if(idCountry.equalsIgnoreCase(JumioConstants.COLOMBIA) || idCountry.equalsIgnoreCase(JumioConstants.PERU)) {
						usuario.setCedula(idNumber != null ? idNumber : usuario.getCedula());
					} else if(idCountry.equalsIgnoreCase(JumioConstants.USA)) {
						usuario.setUsrNss(idNumber != null ? idNumber : usuario.getUsrNss());
					} else if(idCountry.equalsIgnoreCase(JumioConstants.MEXICO)) {
						usuario.setUsrCurp(curp != null ? curp : usuario.getUsrCurp());
					}
					
					user = usuario.getUsrLogin();
					pass = usuario.getUsrPwd();
					nombre = usuario.getUsrNombre();
					apellido = usuario.getUsrApellido();
					correo = usuario.getEMail();
                    telefono = usuario.getUsrTelefono();
					idPais = usuario.getIdPais();
					idApp = usuario.getIdAplicacion();
					idUser = usuario.getIdUsuario();
				} else {
					establecimiento.setJumioStatus(jumioStatus);
					establecimiento.setJumioReference(jumioReference);
					
					establecimiento.setRepresentanteNombre(firstName != null ? firstName : establecimiento.getRepresentanteNombre());
					establecimiento.setRepresentantePaterno(lastName != null ? lastName : establecimiento.getRepresentantePaterno());
					establecimiento.setFechaNacimiento(idDob != null ? idDob : establecimiento.getFechaNacimiento());
					establecimiento.setDocumentNumber(idNumber);
					establecimiento.setDocumentType(idType);
					
					user = establecimiento.getUsuario();
					pass = establecimiento.getPass();
					nombre = establecimiento.getRepresentanteNombre();
					apellido = establecimiento.getRepresentantePaterno();
					correo = establecimiento.getEmailContacto();
                    telefono = establecimiento.getTelefonoContacto();
					idPais = Integer.parseInt(establecimiento.getIdPais());
					idApp = establecimiento.getIdAplicacion();
					idUser = establecimiento.getId();
				}
				
                if(success) {
                    LOGGER.debug("Generando NIP ...");
                    SecureRandom random = new SecureRandom();
                    int num = random.nextInt(100000);
                    nip = String.format("%06d", num); 
                    LOGGER.debug("NIP generado: " + nip);
                    
                    this.sendSMS(nip, telefono, idPais);
                }
                
                if(usuario != null) {
                    usuario.setSmsCode(nip);
                    
                    LOGGER.debug("Actualizando usuario en BD ...");
					TUsuarios usuarioSaved = tUsuariosRepo.save(usuario);
					LOGGER.info("Se ha actualizado correctamente el usuario: " + usuarioSaved.toString());
                } else {
                    establecimiento.setSmsCode(nip);
                    
                    LOGGER.debug("Actualizando establecimiento en BD ...");
					LcpfEstablecimiento establecimientoSaved = establecimientoRepo.save(establecimiento);
					LOGGER.info("Se ha actualizado correctamente el establecimiento: " + establecimientoSaved.toString());
                }
                
                if(idPais == MobileCardConstants.ID_PAIS_EUA) {
					idioma = MobileCardConstants.LANG_ENGLISH;
				}
				
				PushRequest pushReq = new PushRequest();
				pushReq.setId_usuario(idUser);
				pushReq.setIdApp(idApp);
				pushReq.setIdioma(idioma);
				pushReq.setIdPais(idPais);
				pushReq.setModulo(success ? propsFile.getPushModuloExitoso() : propsFile.getPushModuloRechazado());
				pushReq.setTipoUsuario(tipoUsuario);
								
				PushResponse pushResp = (PushResponse) restClient.createRequest(pushReq, propsFile.getPushServiceUrl(), HttpMethod.POST, MobileCardConstants.PUSH_API);
				
				if(pushResp != null && pushResp.getCode() == PushConstants.PUSH_SUCCESS) {
					LOGGER.info("Se ha enviado correctamente la notificacion PUSH");
				} else if(pushResp != null) {
					LOGGER.warn("No se ha podido enviar la notificacion PUSH: " + pushResp.getMessage());
				}
				
				LOGGER.debug("Obteniendo plantilla de correo - ID[{}] Idioma[{}] App[{}]", idMail, idioma, idApp);
				template = templateEmailRepo.
						findByIdAndIdAppAndIdiomaIgnoreCase(idMail, idApp, idioma);
				LOGGER.trace("Plantilla obtenida: " + template.toString());
				
				this.sendEmail(user, pass, nombre, apellido, correo, error, template);
				
				LOGGER.debug("Guardando en BD el callback de Jumio ...");
				JumioValidations jumioValNew = jumioValRepo.save(jumioVal);
				LOGGER.info("Se ha persistido correctamente el callback de Jumio en la BD: " + jumioValNew.toString());
			} catch(IOException ex ) {
				LOGGER.error("Error al parsear parte de la respuesta de Jumio a JSON: " + ex.getMessage());
				ex.printStackTrace();
			} catch(Exception ex ) {
				LOGGER.error("Ocurrio un error desconocido: " + ex.getMessage());
				ex.printStackTrace();
			}
		} else {
			LOGGER.warn("No se encontro ningun usuario/establecimiento con ese jumio_reference");
		}
		
		return success;
	}
	
	private Date convertirFecha(String fecha) {
		SimpleDateFormat formatter = new SimpleDateFormat(JumioConstants.DATE_FORMAT);
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date date = new Date();
		
        try {
        	date = formatter.parse(fecha);
        	LOGGER.debug("Fecha convertida a JAVA: " + fecha);
        } catch (Exception ex) {
        	LOGGER.error("Ocurrio un error al parsear la fecha: " + ex.getMessage());
            ex.printStackTrace();
        }
        
        return date;
	}
	
	private boolean sendSMS(String nip, String numero, int idPais) {
		boolean success = false;
		SMSResponse response = null;
		
		LOGGER.info("Enviando el NIP por SMS al numero: " + numero);
		
		String prefijo = "";
		
		if(idPais == MobileCardConstants.ID_PAIS_MX) {
			LOGGER.debug("Numero de Mexico");
			prefijo = MobileCardConstants.SMS_PREFIX_MX;
		} else if (idPais == MobileCardConstants.ID_PAIS_COL) {
			LOGGER.debug("Numero de Colombia");
			prefijo = MobileCardConstants.SMS_PREFIX_COL;
		} else if (idPais == MobileCardConstants.ID_PAIS_EUA) {
			LOGGER.debug("Numero de EUA");
			prefijo = MobileCardConstants.SMS_PREFIX_EUA;
		} else if (idPais == MobileCardConstants.ID_PAIS_PER) {
			LOGGER.debug("Numero de Peru");
			prefijo = MobileCardConstants.SMS_PREFIX_PER;
		} else {
			LOGGER.warn("El usuario/negocio no tiene definido un idPais en la BD, se enviar� el sms con el prefijo por defecto: "+ MobileCardConstants.SMS_PREFIX_MX);
			prefijo = MobileCardConstants.SMS_PREFIX_MX;
		}
		
		SMSParams smsParams = new SMSParams(nip);
		SMSRequest smsReq = new SMSRequest();
		smsReq.setIdUsuario("");
		smsReq.setIdMensaje(propsFile.getSmsIdMessage());
		smsReq.setNumeroCelular(prefijo + numero);
		smsReq.setParams(smsParams);
		
		try {
			response = (SMSResponse) restClient.createRequest(smsReq, propsFile.getSmsServiceUrl(), HttpMethod.POST, MobileCardConstants.SMS_API);
		} catch (RestClientException | IOException ex) {
			LOGGER.error("Ocurrio un error al consumir la API de SMS: " + ex.getMessage());
			ex.printStackTrace();
		}
		
		if(response != null && response.getCode() == SMSConstants.SMS_SUCCESS_CODE) {
			LOGGER.info("Se envio correctamente el SMS con el NIP");
			success = true;
		} else if(response != null) {
			LOGGER.warn("No se pudo enviar el SMS: " + response.getMessage());
		}
		
		return success;
	}
	
	private boolean sendEmail(String user, String pass, String nombre, String apellido, String correo, String error, TemplateEmail template) {
		boolean success = false;
		LOGGER.info("Enviando correo de notificacion de registro ...");
		
		String value = AddcelCrypto.encryptHard(user) + ":" + AddcelCrypto.encryptHard(pass);
		String param = "?codigo=" + Base64.getEncoder().encodeToString(value.getBytes());
		String url = propsFile.getMcActivationUrl() + param;
		LOGGER.debug("URL de activacion: " + url);
		
		String body = template.getCuerpo();
		body = body.replace(MobileCardConstants.EMAIL_TAG_NOMBRE, nombre + " " + apellido);
		body = body.replace(MobileCardConstants.EMAIL_TAG_PASSWORD, "");
		body = body.replace(MobileCardConstants.EMAIL_TAG_URL, url);
		body = body.replace(MobileCardConstants.EMAIL_TAG_USUARIO, user);
		body = body.replace(MobileCardConstants.EMAIL_TAG_ERROR, error);
		
		CorreoRequest correoReq = new CorreoRequest();
		correoReq.setBody(body);
		correoReq.setCc(new String[] {});
		correoReq.setBcc(new String[] {});
		correoReq.setAttachments(new String[] {});
		correoReq.setCid(new String[] {propsFile.getMcPathImages()});
		correoReq.setFrom(propsFile.getEmailFrom());
		correoReq.setSubject(template.getAsunto());
		correoReq.setTo(new String[] {correo});
		
		try {
			restClient.createRequest(correoReq, propsFile.getEmailServiceUrl(), HttpMethod.POST, MobileCardConstants.EMAIL_API);
		} catch (RestClientException | IOException ex) {
			LOGGER.error("Ocurrio un error al consumir la API de Correos: " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return success;
	}
	
}
