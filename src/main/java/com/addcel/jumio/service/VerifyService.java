/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.addcel.jumio.constants.JumioConstants;
import com.addcel.jumio.constants.MobileCardConstants;
import com.addcel.jumio.constants.StatusConstants;
import com.addcel.jumio.entity.TUsuarios;
import com.addcel.jumio.repository.TUsuariosRepository;
import com.addcel.jumio.response.ApiResponse;
import com.addcel.jumio.response.BaseResponse;
import com.addcel.jumio.util.PropertiesFile;
import com.addcel.jumio.util.RestClient;

@Service
public class VerifyService {

	private static final Logger LOGGER = LogManager.getLogger(VerifyService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	@Autowired
	private TUsuariosRepository tUsuariosRepo;
	
	public ApiResponse verificar(int idApp, int idPais, String idioma, String jumioReference) {
		ApiResponse apiResp = new ApiResponse();
		apiResp.setCode(StatusConstants.SUCCESS_CODE);
		
		try {
			LOGGER.info("Buscando usuario que tenga asignado el jumioReference: " + jumioReference);
			TUsuarios usuario = tUsuariosRepo.findByJumioReference(jumioReference);
			
			if(usuario == null) {
				LOGGER.warn("No hay ningun usuario que tenga asignado ese jumioReference");
				apiResp.setCode(StatusConstants.USER_NOT_FOUND_CODE);
				apiResp.setMessage(StatusConstants.USER_NOT_FOUND_MSG);
				
				return apiResp;
			}
			
			LOGGER.info("Usuario encontrado: " + usuario.toString());
			
			String uri = propsFile.getJumioRestUrl().replaceAll(JumioConstants.URL_WORD_REPLACE, jumioReference);
			BaseResponse baseResp = (BaseResponse) restClient.createRequest(null, uri, HttpMethod.GET, MobileCardConstants.JUMIO_API);
			LOGGER.debug("Respuesta de Jumio: " + baseResp.toString());
			
			if(baseResp.getIdentityVerification() != null && 
					baseResp.getIdentityVerification().getValidity().equalsIgnoreCase(JumioConstants.TRUE_VALIDITY)) {
				
				if(baseResp.getIdentityVerification().getSimilarity().equalsIgnoreCase(JumioConstants.MATCH_SIMILARITY)) {
					LOGGER.info("Jumio ha validado que la persona y documento son validos y coinciden");
					
					usuario.setJumioStatus(MobileCardConstants.JUMIO_APPROVED);
					apiResp.setMessage(JumioConstants.MATCH_SIMILARITY);
					apiResp.setJumioStatus(usuario.getJumioStatus());
				} else {
					LOGGER.warn("Jumio informa que la persona es valida pero no coincide con el documento: " + baseResp.getIdentityVerification().getSimilarity());
					
					usuario.setJumioStatus(MobileCardConstants.JUMIO_DENIED);
					apiResp.setMessage(JumioConstants.NO_MATCH_SIMILARITY);
					apiResp.setJumioStatus(usuario.getJumioStatus());
				} 
			} else if(baseResp.getIdentityVerification() != null && 
					baseResp.getIdentityVerification().getValidity().equalsIgnoreCase(JumioConstants.FALSE_VALIDITY)) {
				LOGGER.warn("Jumio informa que el documento no es valido: " + baseResp.getIdentityVerification().getReason());
				
				usuario.setJumioStatus(MobileCardConstants.JUMIO_DENIED);
				apiResp.setMessage(baseResp.getIdentityVerification().getReason());
				apiResp.setJumioStatus(usuario.getJumioStatus());
			} else if(baseResp.getRejectReason() != null) {
				LOGGER.warn("Jumio informa que la persona no es valida: " + baseResp.getRejectReason().getRejectReasonDescription());
				
				usuario.setJumioStatus(MobileCardConstants.JUMIO_DENIED);
				apiResp.setMessage(baseResp.getRejectReason().getRejectReasonDescription());
				apiResp.setJumioStatus(usuario.getJumioStatus());
			}
			
			LOGGER.debug("Actualizando el jumioStatus del usuario ...");
			TUsuarios usuarioSaved = tUsuariosRepo.save(usuario);
			LOGGER.info("Se ha actualizado correctamente el jumioStatus del usuario: " + usuarioSaved.toString());
		} catch(RestClientException ex) {
			LOGGER.error("Ocurrio un error al consumir el servicio de verify de Jumio: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.JUMIO_NOT_AVAILABLE_CODE);
			apiResp.setMessage(StatusConstants.JUMIO_NOT_AVAILABLE_MSG);
		} catch(Exception ex) {
			LOGGER.error("Ocurrio un error inesperado: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.INTERNAL_ERROR_CODE);
			apiResp.setMessage(StatusConstants.INTERNAL_ERROR_MSG);
		}
		
		return apiResp;
	}
	
}