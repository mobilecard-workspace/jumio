/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.addcel.jumio.constants.JumioConstants;
import com.addcel.jumio.constants.MobileCardConstants;
import com.addcel.jumio.constants.StatusConstants;
import com.addcel.jumio.response.ApiResponse;
import com.addcel.jumio.response.BaseResponse;
import com.addcel.jumio.util.PropertiesFile;
import com.addcel.jumio.util.RestClient;

@Service
public class ExternalVerifyService {
	
	private static final Logger LOGGER = LogManager.getLogger(ExternalVerifyService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private PropertiesFile propsFile;
	
	public ApiResponse verificar(String app, String pais, String jumioReference) {
		ApiResponse apiResp = new ApiResponse();
		apiResp.setCode(StatusConstants.SUCCESS_CODE);
		String message = null;
		
		try {
			String uri = propsFile.getJumioRestUrl().replaceAll(JumioConstants.URL_WORD_REPLACE, jumioReference);
			BaseResponse baseResp = (BaseResponse) restClient.createRequest(null, uri, HttpMethod.GET, MobileCardConstants.JUMIO_API);
			LOGGER.debug("Respuesta de Jumio: " + baseResp.toString());
			
			if(baseResp.getIdentityVerification() != null && 
					baseResp.getIdentityVerification().getValidity().equalsIgnoreCase(JumioConstants.TRUE_VALIDITY)) {
				
				if(baseResp.getIdentityVerification().getSimilarity().equalsIgnoreCase(JumioConstants.MATCH_SIMILARITY)) {
					message = "Jumio ha validado que la persona y documento son validos y coinciden";
					LOGGER.info(message);
				} else {
					message = "Jumio informa que la persona es valida pero no coincide con el documento: " + baseResp.getIdentityVerification().getSimilarity();
					LOGGER.warn(message);
				} 
			} else if(baseResp.getIdentityVerification() != null && 
					baseResp.getIdentityVerification().getValidity().equalsIgnoreCase(JumioConstants.FALSE_VALIDITY)) {
				message = "Jumio informa que el documento no es valido: " + baseResp.getIdentityVerification().getReason();
				LOGGER.warn(message);			
			} else if(baseResp.getRejectReason() != null) {
				message = "Jumio informa que la persona no es valida: " + baseResp.getRejectReason().getRejectReasonDescription();
				LOGGER.warn(message);
			}
			
			apiResp.setMessage(message);
			apiResp.setData(baseResp);
		} catch(RestClientException ex) {
			LOGGER.error("Ocurrio un error al consumir el servicio de verify de Jumio: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.JUMIO_NOT_AVAILABLE_CODE);
			apiResp.setMessage(StatusConstants.JUMIO_NOT_AVAILABLE_MSG);
		} catch(Exception ex) {
			LOGGER.error("Ocurrio un error inesperado: " + ex.getMessage());
			ex.printStackTrace();
			
			apiResp.setCode(StatusConstants.INTERNAL_ERROR_CODE);
			apiResp.setMessage(StatusConstants.INTERNAL_ERROR_MSG);
		}
		
		return apiResp;
	}
	
}
