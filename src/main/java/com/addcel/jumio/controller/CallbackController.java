/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.addcel.jumio.service.CallbackService;
import com.fasterxml.uuid.Generators;

@Controller
public class CallbackController {
	
	@Autowired
	private CallbackService callbackServ;

	private static final Logger LOGGER = LogManager.getLogger(CallbackController.class);
	
	@RequestMapping(value = "/callback")
	@ResponseBody
	public Object callback(@RequestBody String response, HttpServletRequest request) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		
		LOGGER.info("Recibiendo resultado de Jumio desde la IP: " + request.getRemoteAddr());
				
		boolean success = callbackServ.processResult(response, request.getRemoteAddr());
		
		LOGGER.info("Respuesta de Jumio procesada con exito? " + success);
		
		ThreadContext.clearAll();
		return "OK";
	}
	
}
