/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.jumio.constants.StatusConstants;
import com.addcel.jumio.response.ApiResponse;
import com.addcel.jumio.service.VerifyService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class VerifyController {

	@Autowired
	private VerifyService verifyServ;

	private static final Logger LOGGER = LogManager.getLogger(VerifyController.class);
	
	@GetMapping(value = "/{idApp}/{idPais}/{idioma}/verificar/{jumioReference}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object cambioEstado(@PathVariable int idApp, @PathVariable int idPais, @PathVariable String idioma, @PathVariable String jumioReference) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		LOGGER.info("Verificando la validacion del jumioReference: " + jumioReference);
		
		ApiResponse resp = verifyServ.verificar(idApp, idPais, idioma, jumioReference);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha procesado correctamente el verify con Jumio");
		} else {
			LOGGER.warn("No se pudo procesar el verify con Jumio: " + resp.getMessage());
		}
		
		LOGGER.debug("Respuesta regresada al cliente: " + resp.toString());
		
		ThreadContext.clearAll();
		return resp;
	}
	
}
