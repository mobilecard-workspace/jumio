/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.jumio.constants.StatusConstants;
import com.addcel.jumio.response.ApiResponse;
import com.addcel.jumio.service.ExternalVerifyService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api/ext")
public class ExternalVerifyController {
	
	@Autowired
	private ExternalVerifyService extVerifyServ;

	private static final Logger LOGGER = LogManager.getLogger(ExternalVerifyController.class);
	
	@GetMapping(value = "/{app}/{pais}/verify/{jumioReference}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Object verify(@PathVariable String app, @PathVariable String pais, @PathVariable String jumioReference) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		LOGGER.info("Verificando la validacion del jumioReference: " + jumioReference);
		LOGGER.info("App y pais que estan solicitando la verificacion: " + app + " - " + pais);
		
		ApiResponse resp = extVerifyServ.verificar(app, pais, jumioReference);
		
		if(resp.getCode() == StatusConstants.SUCCESS_CODE) {
			LOGGER.info("Se ha procesado correctamente el verify con Jumio");
		} else {
			LOGGER.warn("No se pudo procesar el verify con Jumio: " + resp.getMessage());
		}
		
		LOGGER.debug("Respuesta regresada al cliente: " + resp.toString());
		
		ThreadContext.clearAll();
		return resp;
	}

}
