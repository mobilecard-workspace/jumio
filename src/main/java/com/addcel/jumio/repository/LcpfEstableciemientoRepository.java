/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.jumio.entity.LcpfEstablecimiento;

public interface LcpfEstableciemientoRepository extends CrudRepository<LcpfEstablecimiento, Long> {

	public LcpfEstablecimiento findByJumioReferenceOrId(String jumioReference, Long id);
	
	@SuppressWarnings("unchecked")
	public LcpfEstablecimiento save(LcpfEstablecimiento establecimiento);
	
}
