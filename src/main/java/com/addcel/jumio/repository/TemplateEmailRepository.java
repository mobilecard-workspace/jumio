/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.jumio.entity.TemplateEmail;

public interface TemplateEmailRepository extends CrudRepository<TemplateEmail, String> {

	public TemplateEmail findByIdAndIdAppAndIdiomaIgnoreCase(String id, Integer idApp, String idioma);
	
}
