/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.jumio.entity.JumioValidations;

public interface JumioValidationsRepository extends CrudRepository<JumioValidations, Long> {

	@SuppressWarnings("unchecked")
	public JumioValidations save(JumioValidations jumioValidations);
	
}
