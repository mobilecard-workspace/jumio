/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.jumio.entity.TUsuarios;

public interface TUsuariosRepository extends CrudRepository<TUsuarios, Long> {
	
	public TUsuarios findByJumioReferenceOrIdUsuario(String jumioReference, Long idUsuario);
	
	public TUsuarios findByJumioReference(String jumioReference);
	
	@SuppressWarnings("unchecked")
	public TUsuarios save(TUsuarios usuario);

}
