/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RejectReason {

	private String rejectReasonCode;
	private String rejectReasonDescription;
	
	@JsonIgnore
	private String rejectReasonDetails;
	
}
