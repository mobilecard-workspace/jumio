/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentityVerification {
	
	private String similarity;
	private String validity;
	
	@JsonInclude(Include.NON_NULL)
	private String reason;
	
	@JsonInclude(Include.NON_NULL)
	private Float similarityScore;
	
	@JsonInclude(Include.NON_NULL)
	private String similarityDecision;
	
}
