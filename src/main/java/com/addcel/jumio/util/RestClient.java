/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.addcel.jumio.constants.MobileCardConstants;
import com.addcel.jumio.response.BaseResponse;
import com.addcel.jumio.ws.request.CorreoRequest;
import com.addcel.jumio.ws.response.PushResponse;
import com.addcel.jumio.ws.response.SMSResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	@Autowired
	private PropertiesFile propsFile;
		
	/**
	 * Metodo para consumir una API REST
	 * @param uri url del servicio rest
	 * @return
	 * @throws IOException 
	 * @throws Exception
	 */
	public Object createRequest(Object requestBody, String uri, HttpMethod httpMethod, String app) 
			throws RestClientException, IOException {
		
		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		Object response = null;
		HttpEntity<String> entity = null;
		HttpHeaders headers = null;
		
		LOGGER.debug("Enviando peticion a la API de: " + app);
		LOGGER.debug("{} - {} ", httpMethod.toString(), uri);
		
		switch (app) {
			case MobileCardConstants.JUMIO_API:
				headers = addHeadersJumio(false);
				break;
			case MobileCardConstants.SMS_API:
				headers = addHeadersSMS();
				break;
			case MobileCardConstants.PUSH_API:
				headers = addHeadersPush();
				break;
			case MobileCardConstants.EMAIL_API:
				headers = addHeadersEmail();
				
				HttpEntity<CorreoRequest> request = new HttpEntity<CorreoRequest>((CorreoRequest) requestBody,headers);
				String response2 = restTemplate.postForObject(uri, request, String.class);
				LOGGER.debug("Respuesta de la API de Correos: "  + response2);				
				
				break;
			case MobileCardConstants.IMG_API:
				headers = addHeadersJumio(true);
				break;
			default: headers = addHeadersJumio(false);
				break;
		}
		
		if(requestBody != null) {
			String body = mapper.writeValueAsString(requestBody);
						
			entity = new HttpEntity<String>(body, headers);
			LOGGER.debug("Request Body: " + entity.getBody());
		} else {
			entity = new HttpEntity<String>(headers);
		}
		
		LOGGER.debug("Header: Content-Type - {}", entity.getHeaders().getContentType() != null ?
				entity.getHeaders().getContentType().toString() : "");
		LOGGER.debug("Header: Accept - {}", entity.getHeaders().get("Accept") != null ?
				Arrays.toString(entity.getHeaders().get("Accept").toArray()) : "");
		LOGGER.debug("Header: Authorization - {}", entity.getHeaders().get("Authorization") != null ?
				Arrays.toString(entity.getHeaders().get("Authorization").toArray()) : "");
		
		if(app.equals(MobileCardConstants.IMG_API)) {
			ResponseEntity<byte[]> result = restTemplate.exchange(uri, httpMethod, entity, byte[].class);
			
			LOGGER.debug("Status Code: " + result.getStatusCode());
			response = Base64.getEncoder().encodeToString(result.getBody());
			LOGGER.debug("Body Response en Base64: " + response);
		} else if(app.equals(MobileCardConstants.PUSH_API) || app.equals(MobileCardConstants.SMS_API) ||
				app.equals(MobileCardConstants.JUMIO_API)) {
			ResponseEntity<String> result = restTemplate.exchange(uri, httpMethod, entity, String.class);
			
			LOGGER.debug("Status Code: " + result.getStatusCode());
			LOGGER.debug("Body Response: " + result.getBody());
			
			switch (app) {
				case MobileCardConstants.JUMIO_API:
					response = mapper.readValue(result.getBody(), BaseResponse.class);
					break;
				case MobileCardConstants.SMS_API:
					response = mapper.readValue(result.getBody(), SMSResponse.class);
					break;
				case MobileCardConstants.PUSH_API:
					response = mapper.readValue(result.getBody(), PushResponse.class);
					break;
				default: response = null;
					break;
			}
		}
		
		return response;
	}
	
	/**
	 * 
	 * @param isImage para saber si se va a consumir la API de Jumio de recuperar la imagen del DOC escaneado
	 * @return
	 */
	private HttpHeaders addHeadersJumio(boolean isImage) {
		
		String basicAuth = propsFile.getJumioApiToken() + ":" + propsFile.getJumioApiSecret();
		basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + basicAuth);
		
		if(isImage) {
			LOGGER.debug("Agregando headers para Jumio de la API de Imagen ...");
			headers.setAccept(Arrays.asList(MediaType.IMAGE_JPEG, MediaType.IMAGE_PNG));
			headers.set("User-Agent", MobileCardConstants.APP_VERSION);;
		} else {
			LOGGER.debug("Agregando headers para Jumio de la API de Verify ...");
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		}
		
		return headers;
	}
	
	private HttpHeaders addHeadersSMS() {
		LOGGER.debug("Agregando headers para SMS ...");
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
	
	private HttpHeaders addHeadersEmail() {
		LOGGER.debug("Agregando headers para Email ...");
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
	
	private HttpHeaders addHeadersPush() {
		LOGGER.debug("Agregando headers para Push Notifications ...");
		String basicAuth = propsFile.getPushServiceUsername() + ":" + propsFile.getPushServicePassword();
		basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + basicAuth);
		
		return headers;
	}
	
}
