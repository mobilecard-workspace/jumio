/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${jumio.ip.addres.list}")
	private String jumioIpAddresList;
	
	@Value("${jumio.rest.url}")
	private String jumioRestUrl;
	
	@Value("${jumio.api.token}")
	private String jumioApiToken;
	
	@Value("${jumio.api.secret}")
	private String jumioApiSecret;
	
	@Value("${sms.service.url}")
	private String smsServiceUrl;
	
	@Value("${jumio.validate.ip}")
	private Boolean jumioValidateIp;
	
	@Value("${email.service.url}")
	private String emailServiceUrl;
	
	@Value("${email.from}")
	private String emailFrom;
		
	@Value("${email.registro.id}")
	private String emailRegistroId;
	
	@Value("${email.rechazo.id}")
	private String emailRechazoId;
	
	@Value("${error.msg.photo}")
	private String errorMsgPhoto;
	
	@Value("${error.msg.doc}")
	private String errorMsgDoc;
	
	@Value("${error.msg.match}")
	private String errorMsgMatch;
	
	@Value("${mc.activation.url}")
	private String mcActivationUrl;
	
	@Value("${mc.path.images}")
	private String mcPathImages;
	
	@Value("${push.service.url}")
	private String pushServiceUrl;
	
	@Value("${push.service.username}")
	private String pushServiceUsername;
	
	@Value("${push.service.password}")
	private String pushServicePassword;
	
	@Value("${push.modulo.exitoso}")
	private String pushModuloExitoso;
	
	@Value("${push.modulo.rechazado}")
	private String pushModuloRechazado;
	
	@Value("${sms.id.message}")
	private String smsIdMessage;
	
	@Value("${email.mensaje.edad}")
	private String emailMensajeEdad;
	
}
