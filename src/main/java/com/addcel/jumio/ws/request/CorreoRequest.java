/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CorreoRequest {

	private String[] to;
    private String[] bcc;
    private String[] cc;
    private String[] attachments;
    private Object[] cid;
    private String from;
    private String subject;
    private String body;
	
}
