/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSRequest {

	private String idUsuario;
	private String idMensaje;
	private String numeroCelular;
	private SMSParams params;
	
}
