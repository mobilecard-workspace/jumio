/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.ws.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSParams {

	@JsonProperty("<nip>")
	private String nip;
	
}
