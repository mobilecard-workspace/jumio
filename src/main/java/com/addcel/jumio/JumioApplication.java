/**
 * @author Victor Ramirez
 */

package com.addcel.jumio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class JumioApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(JumioApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(JumioApplication.class, args);
	}
	
}
