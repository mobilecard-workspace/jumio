/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_usuarios")
public class TUsuarios {

	@Id
	@Column(name = "id_usuario", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idUsuario;
	
	@Column(name = "usr_login", nullable = false)
	private String usrLogin;
	
	@Column(name = "usr_nombre", nullable = true)
	private String usrNombre;
	
	@Column(name = "usr_apellido", nullable = true)
	private String usrApellido;
	
	@Column(name = "id_usr_status", nullable = false)
	private Integer idUsrStatus;
	
	@Column(name = "idpais", nullable = true)
	private Integer idPais;
	
	@Column(name = "jumio_status", nullable = true)
	private Integer jumioStatus;
	
	@Column(name = "jumio_reference", nullable = true)
	private String jumioReference;
	
	@Column(name = "usr_telefono", nullable = true)
	private String usrTelefono;
	
	@Column(name = "sms_code", nullable = true)
	private String smsCode;
	
	@Column(name = "eMail", nullable = false)
	private String eMail;
	
	@Column(name = "usr_pwd", nullable = false)
	private String usrPwd;
	
	@Column(name = "id_aplicacion", nullable = true)
	private Integer idAplicacion;
	
	@Column(name = "usr_fecha_nac", nullable = true)
	//@Temporal(TemporalType.DATE)
	private Date usrFechaNac;
	
	@Column(name = "usr_curp", nullable = true)
	private String usrCurp;
	
	@Column(name = "cedula", nullable = true)
	private String cedula;
	
	@Column(name = "usr_nss", nullable = true)
	private String usrNss;
	
	@Column(name = "document_number", nullable = true)
	private String documentNumber;
	
	@Column(name = "document_type", nullable = true)
	private String documentType;
	/*
	@Column(name = "doc_img_front", nullable = true)
	private String docImgFront;
	
	@Column(name = "doc_img_back", nullable = true)
	private String docImgBack;
	*/
}
