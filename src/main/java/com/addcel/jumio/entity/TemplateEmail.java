/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "TEMPLATE_EMAIL")
public class TemplateEmail {

	@Id
	@Column(name = "ID", nullable = false)
	private String id;
	
	@Column(name = "ASUNTO", nullable = false)
	private String asunto;
	
	@Column(name = "CUERPO", nullable = false)
	private String cuerpo;
	
	@Column(name = "ID_APP", nullable = false)
	private Integer idApp;
	
	@Column(name = "IDIOMA", nullable = false)
	private String idioma;
	
}
