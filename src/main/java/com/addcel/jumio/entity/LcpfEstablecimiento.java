/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "LCPF_establecimiento")
public class LcpfEstablecimiento {
	
	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "usuario", nullable = false)
	private String usuario;
	
	@Column(name = "nombre_establecimiento", nullable = true)
	private String nombreEstablecimiento;
	
	@Column(name = "representante_nombre", nullable = true)
	private String representanteNombre;
	
	@Column(name = "representante_paterno", nullable = true)
	private String representantePaterno;
	
	@Column(name = "representante_curp", nullable = true)
	private String representanteCurp;
	
	@Column(name = "fecha_nacimiento", nullable = true)
	private String fechaNacimiento;
	
	@Column(name = "pass", nullable = false)
	private String pass;
		
	@Column(name = "telefono_contacto", nullable = true)
	private String telefonoContacto;
		
	@Column(name = "SMS_CODE", nullable = true)
	private String smsCode;
	
	@Column(name = "email_contacto", nullable = false)
	private String emailContacto;
	
	@Column(name = "jumio_status", nullable = true)
	private Integer jumioStatus;
	
	@Column(name = "jumio_reference", nullable = true)
	private String jumioReference;
	
	@Column(name = "id_pais", nullable = true)
	private String idPais;
	
	@Column(name = "id_aplicacion", nullable = true)
	private Integer idAplicacion;
	
	@Column(name = "document_number", nullable = true)
	private String documentNumber;
	
	@Column(name = "document_type", nullable = true)
	private String documentType;
	
}
