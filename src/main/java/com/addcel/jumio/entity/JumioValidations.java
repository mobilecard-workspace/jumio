/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "jumio_validations")
public class JumioValidations {

	@Id
	@Column(name = "id_jumio_validations", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idJumioValidations;
	
	@Column(name = "response", nullable = false)
	private String response;
	
	@Column(name = "jumio_id_scan_reference", nullable = false)
	private String jumioIdScanReference;
	
	@Column(name = "merchant_id_scan_reference", nullable = true)
	private String merchantIdScanReference;
	
	@Column(name = "id_type", nullable = true)
	private String idType;
	
	@Column(name = "verification_status ", nullable = false)
	private String verificationStatus;
	
	@Column(name = "id_scan_status", nullable = false)
	private String idScanStatus;
	
	@Column(name = "transaction_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;
	
	@Column(name = "callback_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date callbackDate;
	
	@Column(name = "reject_reason_code", nullable = true)
	private String rejectReasonCode;
	
	@Column(name = "reject_reason_description", nullable = true)
	private String rejectReasonDescription;
	
	@Column(name = "similarity", nullable = true)
	private String similarity;
	
	@Column(name = "validity", nullable = true)
	private String validity;
	
	@Column(name = "reason", nullable = true)
	private String reason;
	
	@Column(name = "doc_img_front", nullable = true)
	private String docImgFront;
	
	@Column(name = "doc_img_back", nullable = true)
	private String docImgBack;
	
}
