/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

	private Integer code;
	private String message;
	
	@JsonInclude(Include.NON_NULL)
	private Integer jumioStatus;
	
	@JsonInclude(Include.NON_NULL)
	private BaseResponse data;
	
}
