/**
 * @author Victor Ramirez
 */

package com.addcel.jumio.response;

import com.addcel.jumio.domain.IdentityVerification;
import com.addcel.jumio.domain.RejectReason;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {

	private String scanReference;
	private String mrzCheck;
	private String timestamp;
	
	@JsonInclude(Include.NON_NULL)
	private IdentityVerification identityVerification;
	
	@JsonInclude(Include.NON_NULL)
	private RejectReason rejectReason;
	
}
